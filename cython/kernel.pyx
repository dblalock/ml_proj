#!/bin/env/python

import difflib

def dist(s1, s2):
	# TODO this is probably too slow...
	matcher = difflib.SequenceMatcher(None, s1, s2)
	return 1.0 - matcher.ratio()
