
all:
	python setup.py build_ext --inplace

.PHONY: test, clean

test: all
	nosetests -w test --with-doctest

clean:
	rm python/*.so
	rm -r build/
