# README #

Note that no data of any form is contained within this repository, including intermediate representations that could not even be mapped back to the original dataset. Also excluded is the code that can parse the data, in order to avoid divulging its exact contents. This was done in order to ensure its confidentiality, at the request of our collaborators who graciously provided it.
