#!/bin/env/python

import numpy as np
from joblib import Memory
from collections import Counter

import Data
import sequence as seq
from utils import dictsTo2DArray
from Event import EventTypes

# ================================================================
# Constants
TEAM_KEY = 'TEAM'


# ================================================================
# globals (it's okay, it's just for the joblib decorator!)
memory = Memory('../output', verbose=0)


# ================================================================
# functions

def removeTouches(events):
	"""
	Touches are redundant with real stuff, so we usually want them removed
	"""
	return filter(lambda ev: ev.getType() != EventTypes.TOUCH, events)


def seqsEndingInShots(events, numEventsBefore=0, includeShot=True):
	events = removeTouches(events)

	shot = EventTypes.SHOT
	subSeqLength = numEventsBefore + 1
	seqs = seq.extractSubseqsWhere(lambda subSeq: subSeq[-1].getType() == shot,
		events, subSeqLength)

	if not includeShot and seqs is not None and len(seqs):
		seqs = map(lambda seq: seq[:-1], seqs)

	return seqs


@memory.cache 	# wrapper func so we can cache this
def uniqueSubseqsCountsWithLength(labels, length):
	return seq.uniqueSubseqsCounts(labels, length)


#TODO this should call allTeamsFiles and just go from files -> events
@memory.cache
def allTeamsGames(limit_n_teams=None, limit_n_games=None):
	allGames = []
	teams = []
	i = 0
	for team in Data.getTeams():
		i += 1
		if limit_n_teams and i > limit_n_teams:
			break

		j = 0
		for game in Data.getGamesForTeam(team):
			j += 1
			if limit_n_games and j > limit_n_games:
				break
			game = removeTouches(game)
			for event in game:
				if str(event.team) != team:
					print("THEY NO MATCH!!")
					print("event team: %s, directory team: %s" % (event.team, team))
					print("game id: %d" % event.game)
					assert(0)
			allGames.append(game)
			teams.append(int(team))

	return np.array(allGames), np.array(teams)


def allTeamsFiles(limit_n_teams=None, limit_n_games=None):
	allFiles = []
	teams = []
	i = 0
	for team in Data.getTeams():
		i += 1
		if limit_n_teams and i > limit_n_teams:
			break

		gameFiles = Data.listGameFilesForTeam(team)
		j = 0
		for f in gameFiles:
			j += 1
			if limit_n_games and j > limit_n_games:
				break
			allFiles.append(f)
			teams.append(int(team))
	return np.array(allFiles), np.array(teams)


@memory.cache
def allTeamsGamesLabels():
	"""simple wrapper func to only return labels, since
	these are 100x faster to load"""
	games, teams = allTeamsGames()
	return teams


def seqCountsInGames(games, lengths=[1]):
	gameSeqCounters = []
	for game in games:
		# TODO read in a given symbolization, not raw events
		game = removeTouches(game)
		labels = map(lambda ev: ev.getType(), game)

		counter = Counter()
		for length in lengths:
			counter.update(uniqueSubseqsCountsWithLength(labels, length))
		gameSeqCounters.append(counter)

	return gameSeqCounters


@memory.cache
def seqCountsForTeamGames(team, lengths=[1]):
	games = Data.getGamesForTeam(team) 	# TODO use alphabets, not raw events
	return seqCountsInGames(games, lengths)


@memory.cache
def combinedSeqCounterForTeam(team, lengths=[1]):
	seqCounts = Counter()
	for counter in seqCountsForTeamGames(team, lengths):
		seqCounts.update(counter)
	return seqCounts


@memory.cache
def countsArrayForTeams(lengths=[1]):
	teamCounters = []
	for team in Data.getTeams():
		teamCounter = combinedSeqCounterForTeam(team)
		# teamCounter.pop("ATTACKING MOVE")	# TODO remove hack
		teamCounters.append(teamCounter)
	return dictsTo2DArray(teamCounters)


@memory.cache
def countsArrayForGames(lengths=[1]):
	gameCounters = []
	for team in Data.getTeams():
		counters = seqCountsForTeamGames(team, lengths)
		numGames = len(counters)
		for c in counters:
			# remove sequences that don't occur often enough, as
			# defined by at least once/game on average;
			# we don't iterate thruogh the items directly since
			# we'd be modifying this set as we did so
			items = c.items()
			for key, total in items:
				if total < numGames:	# seems to be about optimal
					c.pop(key)
			c[TEAM_KEY] = team
		gameCounters += counters
	return dictsTo2DArray(gameCounters)


if __name__ == '__main__':
	# Below is a highly last-minute hack to get it to do
	# relative frequencies of different events for each team;
	# it should probably be in figs.py

	# Data.writeAllTeamEvents()
	# sequence counts for teams

	from pandas import DataFrame
	countsMat, colNames = countsArrayForTeams(lengths=[1])
	countsMat = np.array(countsMat, dtype=np.int)
	teams = Data.getTeams()
	# colNames = list(colNames)
	# colNames.remove("ATTACKING MOVE")
	df = DataFrame(countsMat, index=teams, columns=colNames)
	df = df.sort()
	# df = df.drop(["ATTACKING MOVE"],axis=1)
	print(df)

	print(df.sum(axis=1))

	import matplotlib.pyplot as plt
	plt.rcParams["font.size"] = 16

	df_norm = (df - df.min()) / (df.max() - df.min())
	# df_norm = df / df.sum(axis=0)

	plt.figure(figsize=(6.5, 7.5))
	plt.pcolor(df_norm, cmap='Greens')
	plt.colorbar()
	plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
	plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns,
		fontsize=13, rotation=80)

	plt.title("Normalized Event\nCounts for Teams")
	plt.tight_layout()
	plt.savefig("../figs/event_freqs")

	plt.show()
	# printVar("Team total counts", countsMat)
	# printVar("columns",colNames)

	# sequence counts for games
	# countsMat, colNames = countsArrayForGames()
	# printVar("Game counts", countsMat)
	# printVar("columns",colNames)
