#!/bin/env

import numpy as np
import matplotlib.pyplot as plt
import copy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import SVC, LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.qda import QDA
from sklearn.lda import LDA
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.preprocessing import MinMaxScaler, StandardScaler, Normalizer
from sklearn.base import BaseEstimator, TransformerMixin
# from sklearn.feature_selection import SelectKBest
# from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectPercentile
from sklearn.metrics import confusion_matrix

from pandas import DataFrame

import datetime
from collections import defaultdict, Counter
from joblib import Memory
memory = Memory('../output', verbose=0)

import sequence as seq
from utils import extractCols, isSingleton
from soccer import countsArrayForGames, allTeamsFiles
from blocks import *
from Alphabet import Alphabet

CV_FOLDS = 5
EVALUATION_CRITERION = 'accuracy'
# EVALUATION_CRITERION = 'f1'

# @memory.cache 	# this breaks it for no apparent reason...says x has
# shape 1, while y has shape 489 (X has identical shape before and after
# this transformation though)
def tfIdfTransformCols(X):
	return (TfidfTransformer().fit_transform(X.T).T).todense()


class DenseTfidfTransformer(TfidfTransformer):
	# Note that it has to return self, cuz will be called
	# as Foo.fit(...).transform(...)
	def fit(self, X, Y=None, **kwargs):
		return self

	def transform(self, X):
		return tfIdfTransformCols(X)

# TODO use an actual alphabet
@memory.cache
def getAllDataForAlphabet(alphabet=None, lengths=[1]):
	A, labels = countsArrayForGames(lengths)
	teamCol = labels.index(TEAM_KEY)
	Y, X = extractCols(A, teamCol)
	print("data size: %d examples x %d features" % X.shape)
	np.savetxt('../output/X.txt', X, fmt='%d')
	return X, np.array(Y, dtype=np.int)


def zeroOneNormalizeCols(X):
	return MinMaxScaler().fit_transform(X)


def printEstimatorParams(est):
	print("Best params:")
	try:
		print(est.best_estimator_)
	except AttributeError:
		print("N/A")

	print("Grid search scores:")
	try:
		for params, mean_score, scores in est.grid_scores_:
			print("%0.3f (+/-%0.03f) for %r"
				% (mean_score, scores.std() / 2, params))
	except:
		print("N/A")


def printClassificationReport(Ytest, Yhat, classifierName=None):
	print("Classification report:")
	print(classification_report(Ytest, Yhat))
	accuracy = np.sum(Ytest == Yhat) / float(len(Ytest))
	if classifierName:
		if isSingleton(classifierName):
			classifierName = classifierName[0]
		print("%s accuracy: %.3f" % (classifierName, accuracy))
	else:
		print("Classifier Accuracy: %.3f" % (accuracy,))
	print("-------------------------------")

@memory.cache
def calcPrediction(estimator, Xtest):
	"""wrapper for estimator.predict(Xtest) so we can cache the result"""
	return estimator.predict(Xtest)

# ================================================================
# Begin unused but useful code

@memory.cache
def learnEstimator(Xtrain, Ytrain, instance, paramsGrid,
	name, criterion=EVALUATION_CRITERION):
	if paramsGrid:
		print("starting %s grid search..." % (name))
		est = GridSearchCV(instance, paramsGrid, cv=CV_FOLDS,
			scoring=criterion, verbose=2)
		est.fit(Xtrain, Ytrain)
		print("Grid search completed")
		return est

	instance.fit(Xtrain, Ytrain)
	return instance

@memory.cache
def learnRBFSVM(Xtrain, Ytrain, criterion=EVALUATION_CRITERION):
	params = [{'kernel': ['rbf'], 'gamma': [0, 1e-4, 1e-3],
					'C': [1, 10, 100, 1000]}]
	return learnEstimator(Xtrain, Ytrain, SVC(), params, "RBF SVM")

@memory.cache
def learnLinearSVM(Xtrain, Ytrain, criterion=EVALUATION_CRITERION):
	# ^ separate function from RBF SVM to use liblinear
	params = [{'C': [1, 10, 100, 1000]}]
	return learnEstimator(Xtrain, Ytrain, LinearSVC(), params, "Linear SVM")

# @memory.cache
def learnNB(Xtrain, Ytrain, criterion=EVALUATION_CRITERION):
	# params = [{'alpha': [0, 1.0, 10.0]}]	# for multinomial NB (raw counts)
	params = []
	return learnEstimator(Xtrain, Ytrain, GaussianNB(), params, "Multi NB")

def classify(estimCreatFunc, Xtrain, Ytrain, Xtest, Ytest):
	est = estimCreatFunc(Xtrain, Ytrain)
	printEstimatorParams(est)
	Yhat = calcPrediction(est, Xtest)
	printClassificationReport(Ytest, Yhat)

# End unused but useful code
# ================================================================


def name_estimators(estimators):
	"""Generate names for estimators. (Stolen from sklearn pipeline.py)"""

	if isScalar(estimators):
		return type(estimators).__name__

	names = [type(estimator).__name__ for estimator in estimators]
	namecount = defaultdict(int)
	for est, name in zip(estimators, names):
		namecount[name] += 1

	for k, v in list(namecount.iteritems()):
		if v == 1:
			del namecount[k]

	for i in reversed(range(len(estimators))):
		name = names[i]
		if name in namecount:
			names[i] += "-%d" % namecount[name]
			namecount[name] -= 1

	return list(zip(names, estimators))

def getEstimatorNames(pipeline):
	# names, estimators = zip(*pipeline.steps)
	# estimatorClassNames = _name_estimators(estimators)
	# return zip(names, estimatorClassNames)
	_, estimators = zip(*pipeline.steps)
	namesAndDumps = name_estimators(estimators)
	names, _ = zip(*namesAndDumps)
	return names

def stageNames2EstimatorNames(pipeline):
	estNames = getEstimatorNames(pipeline)
	stageNames, _ = zip(*pipeline.steps)
	return dict(zip(stageNames, estNames))


def densifyMat(X):
	return X.toarray()


def removeItemsWhere(conditionFunc, dictionary):
	items = dictionary.items()
	for key, val in items:
		if conditionFunc(key, val):	# seems to be about optimal
			dictionary.pop(key)


def ngramCounts(sequence, ngram_length=1):
	return map(lambda x: seq.uniqueSubseqsCounts(x, ngram_length), sequence)


def combineCounters(counters):
	return reduce(lambda x, y: x + y, counters) # addition sums vals for each key


def countsOfFeatures(counts, keepFeatures):
	numExamples = len(counts)
	numFeatures = len(keepFeatures)
	Xt = np.empty((numExamples, numFeatures))
	for i, count in enumerate(counts):
		for j, feature in enumerate(keepFeatures):
			Xt[i, j] = count.get(feature, 0)
	return Xt


class FrequentFeatureSelector(BaseEstimator, TransformerMixin):

	def __init__(self, ngram_length=1, min_fraction_occurs=1.0):
		# note that these attr names must *exactly* match the param names
		# for this to work with sklearn's CV magic
		self.ngram_length = ngram_length
		self.min_fraction_occurs = float(min_fraction_occurs)

	def fit(self, X, Y=None, **params):
		self.fit_transform(X, Y, **params)
		return self

	def fit_transform(self, X, Y=None, **params):
		# tokenize if passed a document
		if isinstance(X[0], basestring):
			X = map(lambda x: x.split(), X)

		# compute and store item counts for each example
		counts = np.array(ngramCounts(X, self.ngram_length))

		# store counts along with labels so we can do...something...with
		# this info in our report
		self._store_counts(counts, Y)

		# remove features that don't occur at least min_fraction_occurs
		# fraction of the time for at least one label
		self.countsWithinClasses = {}
		combinedCounts = Counter()
		classIdxs = seq.uniqueElementPositions(Y)
		for label, idxs in classIdxs.iteritems():
			countsWithinClass = combineCounters(counts[idxs])
			numExamples = len(idxs)
			minCount = numExamples * self.min_fraction_occurs
			removeItemsWhere(lambda k, v: v < minCount, countsWithinClass)

			self.countsWithinClasses[label] = countsWithinClass
			combinedCounts += countsWithinClass

		# store which features are actually important--this is the
		# real "fitting" in this function.
		self.keepFeatures = sorted(combinedCounts)

		# transform input
		return countsOfFeatures(counts, self.keepFeatures)

	def _transform(self, X):
		assert self.keepFeatures
		print("%d-grams -> %d features"
			% (self.ngram_length, len(self.keepFeatures)))
		if isinstance(X[0], basestring):
			X = map(lambda x: x.split(), X)
		counts = np.array(ngramCounts(X, self.ngram_length))
		return countsOfFeatures(counts, self.keepFeatures)

	def transform(self, X):
		# call this a bizarre way to make joblib happy
		return FrequentFeatureSelector._transform(self, X)
		# return self._transform(X)

	def _store_counts(self, counts, Y):
		labeledCounts = copy.deepcopy(counts)	# don't add class to data
		if Y is not None:
			assert len(Y) == len(counts), "Provided class labels not the" \
				"same length as examples"
			for i, count in enumerate(labeledCounts):
				count[' Class'] = Y[i]	# initial space to come first alphabetically
		self.allCounts = DataFrame.from_records(labeledCounts)
		self.allCounts.fillna(0, inplace=True)

# ======================================================================
# ======================================================================
# --------------- Begin horrible code close to deadline ----------------
# ======================================================================
# ======================================================================

def nowAsString():
	return datetime.datetime.now().strftime("%Y-%m-%dT%H_%M_%S")

@memory.cache
def cachedFit(estimator, X, Y):
	return estimator.fit(X, Y)

# @memory.cache # don't actually cache cuz I think this breaks sklearn's CV
def cachedFitTrainTransform(transforms, Xtrain, Ytrain, Xtest):
	for transform in transforms:
		t = cachedFit(transform, Xtrain, Ytrain)
		Xtrain = t.transform(Xtrain)
		Xtest = t.transform(Xtest)

	return Xtrain, Xtest

@memory.cache
def cachedPredict(estimator, X):
	return estimator.predict(X)

@memory.cache
def runClassifierOnBestParams(X, Y, classifier):
	a = Alphabet(playingZones=12)
	countifier = FrequentFeatureSelector(ngram_length=2)
	norming = DenseTfidfTransformer()
	selector = SelectPercentile(percentile=50)

	# apply all the transforms that come before the classifier
	Xtrain, Xtest, Ytrain, Ytest = stratifiedSplitTrainTest(X, Y)
	transforms = (a, countifier, norming, selector)
	Xtrain, Xtest = cachedFitTrainTransform(transforms, Xtrain, Ytrain, Xtest)

	# fit and run classifier
	p = cachedFit(classifier, Xtrain, Ytrain)
	Yhat = cachedPredict(p, Xtest)
	return Ytest, Yhat

def legibleClassifierName(classifier):
	name = name_estimators(classifier)
	if name == "DecisionTreeClassifier":
		name = "CART"
	elif name == "SVC":
		name = "RBF SVM"
	elif name == "LinearSVC":
		name = "Linear SVM"
	elif name == "LogisticRegression":
		name = "Logistic Regression"
	return name

def confusionMat(classifier, save=True):
	X, Y = allTeamsFiles()

	# ================================================================
	# mess around a little to see if we can beat the best settingss
	if 0:
		alphabetParams = [{'playingZones': [8], 'passTypes':[1, 2]}]
		featureCountParams2 = [{'ngram_length': [2]}]
		featureCountParams3 = [{'ngram_length': [3]}]
		selectPctParams = [{'percentile': [50]}]
		linearParams = [{'C': [1, 10]}]
		# lrParams = [{'C': [1, 10]}]

		symbolizing = [(Alphabet(), alphabetParams)]
		countifying = [(FrequentFeatureSelector(), featureCountParams2),
						(FrequentFeatureSelector(), featureCountParams3)]
		selectPct = (SelectPercentile(), selectPctParams)
		colNorming = [DenseTfidfTransformer(), StandardScaler()]
		featureSelecting = [selectPct, None]

		# lr = (LogisticRegression(random_state=12345), lrParams)
		linearSVM = (LinearSVC(), linearParams)
		classifiers = [linearSVM]

		d = [("symbolize", symbolizing),	# TODO sweep alphabet params
			("countify", countifying),
			# ("rowNorming", rowNorming),
			("colNorming", colNorming),
			("featureSelect", featureSelecting),
			('classify', classifiers)]

		outcomes = tryParams(d, X, Y, cacheBlocks=False)
		print(outcomes)
		outcomes.to_csv('../results/outcomes_%s.csv' % (nowAsString()))

	# ================================================================
	# best settings
	if 0:
		alphabetParams = [{'playingZones': [12], 'passTypes': [1]}]
		featureCountParams2 = [{'ngram_length': [2]}]
		selectPctParams = [{'percentile': [50]}]
		linearParams = [{'C': [10]}]

		# best pipeline
		symbolizing = [(Alphabet(), alphabetParams)]
		countifying = [(FrequentFeatureSelector(), featureCountParams2)]
		selectPct = (SelectPercentile(), selectPctParams)
		colNorming = [DenseTfidfTransformer()]
		featureSelecting = [selectPct]

		linearSVM = (LinearSVC(), linearParams)
		classifiers = [linearSVM]

		# run it
		d = [("symbolize", symbolizing),	# TODO sweep alphabet params
			("countify", countifying),
			# ("rowNorming", rowNorming),
			("colNorming", colNorming),
			("featureSelect", featureSelecting),
			('classify', classifiers)]

		outcomes = tryParams(d, X, Y, cacheBlocks=False)
		print(outcomes)
		outcomes.to_csv('../results/outcomes_%s.csv' % (nowAsString()))

	# ================================================================
	# run stuff directly
	if 1:
		Ytest, Yhat = runClassifierOnBestParams(X, Y, classifier)

		printClassificationReport(Ytest, Yhat)
		mat = confusion_matrix(Ytest, Yhat)

		# pretty confident wrapping this in a dataframe is pointless, but
		# refactoring pasted code from the interwebs isn't a priority right now
		classes = sorted(seq.uniqueElements(Ytest))
		df = DataFrame(mat, index=classes, columns=classes)
		# sns.heatmap(df, annot=True)
		# sns.heatmap(df, annot=False)
		fig, ax = plt.subplots(1)
		# ppl.pcolormesh(fig, ax, mat)
		plt.pcolor(df, cmap='Greens')
		# plt.pcolor(df, cmap='BuGn')
		# plt.pcolor(df, cmap='binary')
		plt.colorbar()
		plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
		plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns, rotation=80)

		name = legibleClassifierName(classifier)
		plt.rcParams["font.size"] = 18
		plt.title(name + " Confusion Matrix")
		plt.savefig('../figs/confusionMat_' + name)

		# plt.show()


def removeClasses(X, Y, classes):
	for team in classes:
		idxs = np.where(Y == team)
		X = np.delete(X, idxs, axis=0)
		Y = np.delete(Y, idxs)
	return X, Y

def removeWeirdOnes(X, Y):
	weirdOnes = (156, 683)
	return removeClasses(X, Y, weirdOnes)

def confusionMats(classifiers, ignoreWeirdOnes=False):
	if isScalar(classifiers):
		classifiers = [classifiers]
	nClassifiers = len(classifiers)

	X, Y = allTeamsFiles()
	if ignoreWeirdOnes:
		X, Y = removeWeirdOnes(X, Y)

	plt.rcParams["font.size"] = 18

	width = 4 + len(classifiers)
	height = 5 * len(classifiers)
	plt.figure(figsize=(width, height))
	for i, classifier in enumerate(classifiers):
		plt.subplot(nClassifiers, 1, i+1)
		Ytest, Yhat = runClassifierOnBestParams(X, Y, classifier)

		printClassificationReport(Ytest, Yhat)
		mat = confusion_matrix(Ytest, Yhat)
		classes = sorted(seq.uniqueElements(Ytest))

		df = DataFrame(mat, index=classes, columns=classes)
		plt.pcolor(df, cmap='Greens')
		plt.colorbar()
		plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
		plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns, rotation=80)

		name = legibleClassifierName(classifier)
		plt.title(name)

	if ignoreWeirdOnes:
		plt.suptitle("Confusion Matrices,\nProblem Classes Removed", fontsize=26)
		plt.tight_layout()
		if len(classifiers) == 2:
			plt.subplots_adjust(top=0.85)
		elif len(classifiers) == 3:
			plt.subplots_adjust(top=0.91)
	else:
		plt.suptitle("Confusion Matrices", fontsize=26)
		plt.tight_layout()
		if len(classifiers) == 2:
			plt.subplots_adjust(top=0.9)
		elif len(classifiers) == 3:
			plt.subplots_adjust(top=0.93)
	path = '../figs/confusionMat_combined'
	if ignoreWeirdOnes:
		path = path + '_noWeird'
	plt.savefig(path)

def runExperiments():
	X, Y = allTeamsFiles()
	numClasses = len(seq.uniqueElements(Y))

	# alphabetParams = [{'success': (False, True),
	alphabetParams = [{
							# 'playingZones': [2, 4, 8, 12]}]
							'playingZones': [2, 4, 8, 12, 16],
							# 'passParam': [1, 2]}]
							'passParam': [1, 4],
							# 'passTypes': [1],
							'passTypes': [2, 3]}]
							# 'shotTypes': [1],
							# 'shotLocations': [1]}]
	# alphabetParams = [{'success': (False, True)}]
	# featureCountParams = [{'ngram_length': [1, 2, 3, 4]}]
	featureCountParams1 = [{'ngram_length': [1]}]
	featureCountParams2 = [{'ngram_length': [2]}]
	featureCountParams3 = [{'ngram_length': [3]}]
	featureCountParams4 = [{'ngram_length': [4]}]
	# selectPctParams = [{'percentile': [10, 20, 50]}]
	# selectPctParams = [{'percentile': [25, 50]}]
	selectPctParams = [{'percentile': [50]}]
	rbfParams = [{'C': [1, 10, 100, 1000]}]
	linearParams = [{'C': [1, 10, 100, 1000]}]
	lrParams = [{'C': [1, 10, 100, 1000]}]
	# nnParams = [{'weights': ['uniform', 'distance'], 'n_neighbors': [3, 5]}]
	nnParams = [{'weights': ['distance'], 'n_neighbors': [7, 9]}]
	# nnParams = [{'n_neighbors': [1]}]

	selectPct = (SelectPercentile(), selectPctParams)
	rbf = (SVC(), rbfParams)
	linearSVM = (LinearSVC(), linearParams)
	lr = (LogisticRegression(random_state=12345), lrParams)
	nn = (KNeighborsClassifier(), nnParams)
	# lda = LDA()

	symbolizing = [(Alphabet(), alphabetParams)]
	# countifying = [(FrequentFeatureSelector(), featureCountParams)]
	countifying = [(FrequentFeatureSelector(), featureCountParams1),
					(FrequentFeatureSelector(), featureCountParams2),
					# (FrequentFeatureSelector(), featureCountParams3)]
					(FrequentFeatureSelector(), featureCountParams3),
					(FrequentFeatureSelector(), featureCountParams4)]
	rowNorming = [Normalizer(norm='l1'), None]
	# colNorming = [DenseTfidfTransformer(), StandardScaler(), None]
	# colNorming = [StandardScaler(), None]
	# colNorming = [DenseTfidfTransformer()]
	colNorming = [DenseTfidfTransformer(), StandardScaler(), None]
	featureSelecting = [selectPct, None]

	# results reported are from running each of these in succession (we added
	# one and then commented the previous one). The results match up with
	# what's in results/outcomes_... on 12/4/14 and, more easily, with what's
	# in results.numbers
	# classifiers = [GaussianNB()]
	# classifiers = [LinearSVC()]
	# classifiers = [SVC()]
	# classifiers = [LogisticRegression(random_state=12345)]
	# classifiers = (SVC(), LinearSVC(), LogisticRegression(random_state=12345), GaussianNB())
	# classifiers = [LDA()]
	# classifiers = (rbf, linearSVM, lr, GaussianNB())
	# classifiers = [DecisionTreeClassifier()]
	# classifiers = [[AdaBoostClassifier(), {}]]
	# classifiers = [nn]
	# classifiers = [rbf]


	d = [("symbolize", symbolizing),	# TODO sweep alphabet params
		("countify", countifying),
		# ("rowNorming", rowNorming),
		("colNorming", colNorming),
		("featureSelect", featureSelecting),
		('classify', classifiers)]

	outcomes = tryParams(d, X, Y, cacheBlocks=False)
	print(outcomes)
	outcomes.to_csv('../results/outcomes_%s.csv' % (nowAsString()))

if __name__ == '__main__':
	print("about to classify the crap out of some stuff")

	if 1:	# create confusion matrices
		# confusionMat(LinearSVC(C=10))
		# confusionMat(LogisticRegression(C=1))
		# confusionMat(SVC(C=10))
		# confusionMat(GaussianNB())
		# confusionMat(DecisionTreeClassifier())

		# confusionMats(LinearSVC(C=10))
		# confusionMats((LinearSVC(C=10), SVC(C=10), GaussianNB()))
		confusionMats((LinearSVC(C=10), SVC(C=10)))
		# confusionMats((LinearSVC(C=10), SVC(C=10), GaussianNB()),
			# ignoreWeirdOnes=True)
		confusionMats((LinearSVC(C=10), SVC(C=10)), ignoreWeirdOnes=True)

		import sys
		sys.exit()

	if 0:
		runExperiments()
		import sys
		sys.exit()

	if 0:	# test FrequentFeatureSelector
		mine = FrequentFeatureSelector(min_fraction_occurs=0)
		theirs = CountVectorizer()

		# X, Y = allTeamsFiles(limit_n_teams=2, limit_n_games=12)
		# a = Alphabet()
		# strs = a.transform(X)
		strs = ['aa aa bb', 'aa aa cc bb', 'bb aa bb cc', 'bb cc cc bb']
		Y = [0, 0, 1, 1]

		myOut = mine.fit_transform(strs, Y)
		theirOut = densifyMat(theirs.fit_transform(strs, Y))
		printVar("mine", myOut)
		printVar("theirs", theirOut)

		import sys
		sys.exit()
