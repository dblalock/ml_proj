#!/bin/env/python

import matplotlib.pyplot as plt
from matplotlib.colors import cnames as colors
from matplotlib.path import Path
import matplotlib.patches as patches

from Event import EventTypes
from soccer import seqsEndingInShots
from Data import getGamesAsEventSeqs

X_YDS = 120.
Y_YDS = 100.
EVENT_X_MAX = 100.
EVENT_Y_MAX = 100.

FIELD_FIG_WIDTH = 12./2
FIELD_FIG_HEIGHT = 8./2
YDS_TO_FIG_X = FIELD_FIG_WIDTH / X_YDS
YDS_TO_FIG_Y = FIELD_FIG_HEIGHT / Y_YDS

def addRectangle(ax, topLeft, bottomRight, color='white', linewidth=2):
	topRight = (bottomRight[0], topLeft[1])
	bottomLeft = (topLeft[0], bottomRight[1])
	verts = [bottomLeft,
			topLeft,
			topRight,
			bottomRight,
			bottomLeft]
	codes = [Path.MOVETO,
			Path.LINETO,
			Path.LINETO,
			Path.LINETO,
			Path.CLOSEPOLY]
	path = Path(verts, codes)
	patch = patches.PathPatch(path, facecolor=color, lw=linewidth)
	ax.add_patch(patch)
	return patch

def colorForEventType(typ):
	t = EventTypes 		# abbreviation
	if typ == t.SHOT:
		return 'g'
	elif typ == t.PASS:
		return colors['gray']
	elif typ == t.TOUCH:
		return colors['brown']
	elif typ == t.CLEARANCE:
		return 'c'
	elif typ == t.TAKE_ON or typ == t.CHALLENGE or typ == t.ATTACKING_MOVE:
		return colors['magenta']
	elif typ == t.INTERCEPTION:
		return colors['orange']
	elif typ == t.FOUL:
		return 'y'
	# print("coloring mystery type: %s" % typ)
	return 'k'

def scalePositions(xPositions, yPositions, width, height):
	xScale = X_YDS / EVENT_X_MAX
	yScale = Y_YDS / EVENT_Y_MAX
	xPositions = map(lambda x: x * xScale, xPositions)
	yPositions = map(lambda y: y * yScale, yPositions)
	return xPositions, yPositions

def plotSoccerField(width=FIELD_FIG_WIDTH, height=FIELD_FIG_HEIGHT,
	ax=None):
	if ax is None:
		fig = plt.figure(figsize=(width, height))
		ax = fig.add_subplot(111)

	boxWidth = 18. * YDS_TO_FIG_Y
	boxHeight = 44. * YDS_TO_FIG_X
	boxTop = (height / 2) - (boxHeight / 2)
	boxBottom = (height / 2) + (boxHeight / 2)
	midfield = width / 2

	# add left box
	topLeft = [0, boxTop]
	bottomRight = [boxWidth, boxBottom]
	addRectangle(ax, topLeft, bottomRight)

	# add right box
	topLeft[0] = width - boxWidth
	bottomRight[0] = width
	addRectangle(ax, topLeft, bottomRight)

	# add midfield line
	ax.plot([midfield, midfield], [0, height], color='k', lw=2)

	return ax

def calcBallPath(events):
	xPositions = map(lambda ev: ev.start_x, events)
	yPositions = map(lambda ev: ev.start_y, events)
	eventTypes = map(lambda ev: ev.getType(), events)

	# add in end positions for passes
	newX = []
	newY = []
	newTypes = []
	for i, typ in enumerate(eventTypes):
		newX.append(xPositions[i])
		newY.append(yPositions[i])
		newTypes.append(typ)
		if typ == EventTypes.PASS:
			event = events[i]
			x = event.end_x
			y = event.end_y
			try:
				if x != xPositions[i+1] and y != yPositions[i+1]:
					newX.append(event.end_x)
					newY.append(event.end_y)
					# treat whatever is between a pass and the
					# next thing as a mystery with its own name
					newTypes.append("Post-Pass")
			except IndexError:
				pass 	# ask for forgiveness if i+1 out of range, not permission

	xPositions = newX
	yPositions = newY
	eventTypes = newTypes

	# add the position where the ball went after a shot if the last
	# event is a shot
	if eventTypes[-1] == EventTypes.SHOT:
		shotEvent = events[-1]

		# figure out which side they were shooting at
		shotX = float(shotEvent.start_x)
		endX = round(shotX / EVENT_X_MAX) * EVENT_X_MAX

		shotY = shotEvent.goal_y
		if shotY != 0:			# ignore blocked shots, logged as 0
			xPositions.append(endX)
			yPositions.append(shotEvent.goal_y)
	return xPositions, yPositions, eventTypes

def plotBallPath(xPositions, yPositions, eventTypes=None, ax=None):
	"""
	Plot the path of the ball, as defined by the sequence of n xPositions
	and n yPositions. The n-1 lines shown are colored based on the
	eventTypes provided.

	>>> x = (80, 110, 120)
	>>> y = (45, 55, 55)
	>>> plotBallPath(x, y)

	>>> from Event import EventTypes as types
	>>> eventTypes = [types.PASS, types.PASS, types.SHOT]
	>>> plotBallPath(x, y, eventTypes)
	"""
	assert(len(xPositions) == len(yPositions))
	if eventTypes is not None:
		assert(len(xPositions) - 1 <= len(eventTypes))

	width = FIELD_FIG_WIDTH
	height = FIELD_FIG_HEIGHT

	if ax is None:
		ax = plotSoccerField()

	# plot path of ball
	xPositions, yPositions = scalePositions(xPositions, yPositions,
		width, height)
	xPositions = map(lambda x: x * YDS_TO_FIG_X, xPositions)
	yPositions = map(lambda y: y * YDS_TO_FIG_Y, yPositions)
	if eventTypes is None:
		ax.plot(xPositions, yPositions, '-', color='y', lw=2)
	else:
		colors = map(colorForEventType, eventTypes)
		for i in range(len(xPositions) - 1):
		# for i in range(1):
			dx = [xPositions[i], xPositions[i + 1]]
			dy = [yPositions[i], yPositions[i + 1]]
			# print(colors[i])
			# print(dx)		# there seems to be a touch logged on top of every pass...
			ax.plot(dx, dy, '-', color=colors[i], lw=2)

	ax.set_xlim(0, width)
	ax.set_ylim(0, height)

def plotEvents(events, ax=None):
	plotBallPath(*calcBallPath(events), ax=ax)


# QUESTIONS FOR MATT:
	# Is the shot_y using the same scale as the whole field?
		# Everything appears to just be in the lower 2 thirds
	# Are we good as far as removing unneeded "touches"?
		# Because the thing before a shot is always a "touch", and sometimes
		# a very long one...


def plotGames(maxGames=None, show=True):
	ax = plotSoccerField()
	for i, eventSeq in enumerate(getGamesAsEventSeqs()):
		# print(i)

		numEventsBefore = 2
		shotSeqs = seqsEndingInShots(eventSeq, numEventsBefore)
		# for seq in shotSeqs[:10]:
		for s in shotSeqs:
			plotEvents(s, ax=ax)

		if maxGames is not None and i == (maxGames-1):
			break

	if show:
		plt.show()

def plotEventTypesForTeams():
	# from bokeh.sampledata.unemployment1948 import data
	from bokeh.sampledata import __all__ as stuff

	print(stuff)
	return

	# data = unemployment1948.data
	# # pandas magic
	# df = data[data.columns[:-2]]
	# df2 = df.set_index(df[df.columns[0]].astype(str))
	# df2.drop(df.columns[0], axis=1, inplace=True)
	# df3 = df2.transpose()

	# bokeh magic
	from bokeh.charts import CategoricalHeatMap
	hm = CategoricalHeatMap(df3, title="categorical heatmap, pd_input", filename="cat_heatmap.html")
	hm.width(1000).height(400).show()

if __name__ == '__main__':
	# import doctest
	# doctest.testmod()

	# plotGames()

	plotEventTypesForTeams()
