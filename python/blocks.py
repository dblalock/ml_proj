#!/bin/env/python

import itertools
import copy
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import StratifiedKFold
from sklearn.base import BaseEstimator, TransformerMixin

from pandas import DataFrame

from joblib import Memory
memory = Memory('../output', verbose=1)


def isScalar(x):
	return not hasattr(x, "__len__")


def cleanKeys(pipeParamsDict):
	"""go from, e.g., 'classify__C' to 'C'"""
	dictionary = {}
	for key, val in pipeParamsDict.iteritems():
		newKey = key.split('__', 1)[1]
		dictionary[newKey] = val
	return dictionary


def add_caching_to_funcs(obj, funcNames):
	mem = Memory('../.add_caching_to_funcs', verbose=11)
	if obj is None or funcNames is None:
		return
	if isScalar(funcNames):
		funcNames = [funcNames]
	for name in funcNames:
		func = getattr(obj, name, None)
		if func is not None:
			setattr(obj, name, mem.cache(func))


def add_caching(estimator):
	return add_caching_to_funcs(estimator,
		['transform', 'fit', 'fit_transform', 'predict'])


cached_apply_mem = Memory('../.cached_apply', verbose=0)
@cached_apply_mem.cache
def cached_apply(func, X):
	return func(X)


class FuncWrapper(BaseEstimator, TransformerMixin):

	@staticmethod
	def wrap(func):
		return FuncWrapper(func)

	# BaseEstimator figures out what our params are based on the signature
	# of init, so we have to list them all here (though in this case it's
	# just the funciton we're wrapping)
	def __init__(self, func, cache=True):
		self.func = func
		self.cache = cache

	def __call__(self):	# so this works as a func that returns a transform
		return self

	def fit(self, X, y=None, **params):
		return self

	def transform(self, X):
		if self.cache:
			return cached_apply(self.func, X)
		return self.func(X)


def _prependStringToKeys(string, dictionary):
	for key in dictionary.keys():
		val = dictionary.pop(key)
		newKey = ''.join((string, key))
		dictionary[newKey] = val


def makePipelines(stages, cacheBlocks=False):
	# should make this really good and then pull request to sklearn

	stageNames, _ = zip(*stages)	# first element of each pair

	funcsForStages = []
	for i in range(len(stages)):
		funcsForStages.append([])

	for i, (stageName, funcsWithParams) in enumerate(stages):

		# if we got, eg, ("classify", SVC), make it ("classify", [SVC])
		if (isScalar(funcsWithParams)):
			funcsWithParams = [funcsWithParams]

		for funcAndParams in funcsWithParams:	# eg (SVC, ['C':[1, 5]])

			# if it's just, say SVC, or None, then use default parameters
			if isScalar(funcAndParams):
				func = funcAndParams
				# if func is not None:
				funcsForStages[i].append((func, {}))

			# otherwise, it's a function with something we assume can be made
			# into a ParamsGrid (and it's the user's fault if it can't).
			# To leverage sklearn Pipeline's ability to work with GridSearchCV,
			# we need to have all the keys in the parameters be of the form
			# stageName__paramName; e.g., for stage name "dimReduction" and
			# parameter name "n_dimensions", we want "dimReduction__n_dimensions"
			else:
				assert len(funcAndParams) == 2, \
					"Bad (function, params) given; neither scalar nor length 2: %s" \
					% (str(funcAndParams),)

				func = funcAndParams[0]
				params = funcAndParams[1]
				prefix = ''.join((stageName, '__'))

				# if it's just one level of dict, change keys directly
				if isinstance(params, dict):
					_prependStringToKeys(prefix, params)
					funcsForStages[i].append((func, params))

				# otherwise, it must be a collection of dicts
				else:
					for d in params:
						assert isinstance(d, dict), \
							"Parameter element %s not a dict!" % str(d)
						_prependStringToKeys(prefix, d)
						funcsForStages[i].append((func, d))

	# now we take the cartesian product of all funcs at all stages
	# and store the result from each, along with the parameters;
	allPipelines = []
	allParams = []
	allCombos = itertools.product(*funcsForStages)
	for combo in allCombos:
		objs = []
		comboParams = {}
		for c in combo:
			func = c[0]
			params = c[1]
			if func is not None:
				comboParams.update(params)
			objs.append(func)

		stagesNotNone = [(stageName, obj) for stageName, obj in zip(stageNames, objs)
			if obj is not None]
		try:
			# this will only work with my modified Pipeline class, which hasn't been
			# pulled into sklearn yet
			allPipelines.append(Pipeline(stagesNotNone, cache=cacheBlocks))
		except TypeError:
			allPipelines.append(Pipeline(stagesNotNone))
		allParams.append(comboParams)

	return allPipelines, allParams

@memory.cache
def gridSearchPipeline(pipeline, paramsGrid, Xtrain, Ytrain, **cvParams):
	print("Grid Searching pipeline:")
	print(pipeline)

	# use 5-fold stratified cross-validation by default to maintain
	# consistent class balance across training and testing
	if 'cv' not in cvParams:
		cvParams['cv'] = StratifiedKFold(Ytrain, n_folds=5)

	cv = GridSearchCV(pipeline, paramsGrid, **cvParams)
	cv.fit(Xtrain, Ytrain)
	return cv

@memory.cache
def stratifiedSplitTrainTest(X, Y, n_folds=4):
	split = StratifiedKFold(Y, n_folds=n_folds, random_state=12345)
	train_index, test_index = next(iter(split))
	X, Xtest = X[train_index], X[test_index]
	Y, Ytest = Y[train_index], Y[test_index]
	return X, Xtest, Y, Ytest

def params2scores(d, X, Y, n_folds=4, scoring='accuracy', cacheBlocks=False):
	# split into training and test sets; we use a stratified split
	# to maintain class balance; more folds -> more training
	X, Xtest, Y, Ytest = stratifiedSplitTrainTest(X, Y)

	allBestParams = []
	allScores = []
	pipelines, pipeParams = makePipelines(d, cacheBlocks=cacheBlocks)
	for pipe, params in zip(pipelines, pipeParams):
		cv = gridSearchPipeline(pipe, params, X, Y,
			scoring=scoring, n_jobs=3, verbose=1)

		bestParams = stageNames2EstimatorNames(pipe)
		bestParams.update(cleanKeys(cv.best_params_))

		score = cv.score(Xtest, Ytest)
		allBestParams.append(bestParams)
		allScores.append(score)

	return allScores, allBestParams

def createScoreAndParamsTable(scores, params, scoreName='Score'):
	# add a leading space to ensure score is first alphabetically
	scoreName = ' ' + scoreName

	# add score to the data that will constitute the table; we first
	# make a copy of the parameters though so as not to modify the
	# collection passed in; since this is a collection of dicts that
	# we don't want to alter, we need a deep copy
	params = copy.deepcopy(params)
	for score, p in zip(scores, params):
		p[scoreName] = score

	outcomes = DataFrame.from_records(params) # pandas magically eats dicts
	outcomes.sort(inplace=True)								# sort by col name
	outcomes.sort(scoreName, ascending=False, inplace=True)	# sort by score
	return outcomes


def tryParams(d, X, Y, n_folds=4, scoring='accuracy', cacheBlocks=False):
	scores, bestParams = params2scores(d, X, Y,
		n_folds=n_folds, scoring=scoring, cacheBlocks=cacheBlocks)
	return createScoreAndParamsTable(scores, bestParams, scoring)


if __name__ == '__main__':
	pass
	# from classify import *
	# from sklearn.decomposition import PCA
	# from sklearn.svm import SVC, LinearSVC
	# from sklearn.feature_selection import SelectKBest
	# from sklearn.feature_selection import chi2

	# X, Y = getAllDataForAlphabet(lengths=[1])

	# featureSelect = (SelectKBest, {'score_func': [chi2],'k': (5, 10)})

	# svmParams = [{'kernel': ['rbf'], 'gamma': [0, 1e-4, 1e-3],
	# 				'C': [1, 10, 100, 1000]}]
	# linearSVMparams = [{'C': [1, 10, 100, 1000]}]
	# classifiers = [(SVC, svmParams), (LinearSVC, linearSVMparams)]

	# d = [("featureSelect", [featureSelect, None]),
	# 	("dimRed", PCA),
	# 	("classify", classifiers)]
	# pipelines, pipeParams = makePipelines(d)
	# print '================================'

	# cvFolds = 5
	# for pipe, params in zip(pipelines, pipeParams):
	# 	cv = GridSearchCV(pipe, params, cv=cvFolds,
	# 		scoring='accuracy', n_jobs=2, verbose=1)
	# 	cv.fit(X, Y)
	# 	cv.predict(X)
