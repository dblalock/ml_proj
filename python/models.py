#!/bin/env/python

import numpy as np
import itertools

from utils import printVar	# TODO remove after debug


# ================================================================
# Interfaces

class Model(object):
	"""Generic estimator interface used for parameter tuning"""
	def setTrainData(self, X, Y):
		pass

	def setParams(self, **kwargs):
		pass

	def train(self):
		pass

	def estimate(self, x):
		pass

	def estimates(self, X):
		return map(self.estimate, X)


# ================================================================
# Loss functions

def hingeLoss(yHat, y):
	yHat = np.array(yHat)
	y = np.array(y)
	# printVar("hingeloss: Yhat",yHat)
	# printVar("hingeloss: Y",y)
	return np.sum(yHat != y)


def absLoss(yHat, y):
	yHat = np.array(yHat)
	y = np.array(yHat)
	return np.sum(np.abs(yHat-y))


def squaredLoss(yHat, y):
	yHat = np.array(yHat)
	y = np.array(yHat)
	return np.norm(yHat-y)**2


# ================================================================
# Parameter tuning functions

def computeLoss(model, X, Y, lossFunc=hingeLoss):
	yHats = model.estimates(X)
	return lossFunc(yHats, Y)


def findBestParams(model, paramsDict, Xtrain, Ytrain, Xval, Yval,
	lossFunc=hingeLoss):
	model.setTrainData(Xtrain, Ytrain)

	# grid search: cartesian product of all parameter values
	bestValidationLoss = np.inf
	bestParams = None
	paramNames = paramsDict.keys()
	for p in itertools.product(*paramsDict.values()):
	# for p in itertools.product(*paramValues):
		params = dict(zip(paramNames, p))
		model.setParams(**params)
		print("training model with params:")
		print(params)
		model.train()
		print("computing loss on validation set...")
		loss = computeLoss(model, Xval, Yval, lossFunc)
		print("loss = %g" % loss)
		if loss < bestValidationLoss:
			bestValidationLoss = loss
			bestParams = params

	return bestParams, bestValidationLoss


def calcTrainValTestLoss(model, paramsDict, Xtrain, Ytrain,
	Xval, Yval, Xtest, Ytest, lossFunc=hingeLoss):

	bestParams, valLoss = findBestParams(model, paramsDict,
		Xtrain, Ytrain, Xval, Yval, lossFunc)

	printVar("bestParams", bestParams)

	if bestParams != paramsDict:	# more than one parameter
		model.setParams(**bestParams)
		model.train()
	print("computing loss on training set using best parameters...")
	trainLoss = computeLoss(model, Xtrain, Ytrain, lossFunc)
	print("computing loss on test set using best parameters...")
	testLoss = computeLoss(model, Xtest, Ytest, lossFunc)

	return trainLoss, valLoss, testLoss
