#!/bin/env

import operator
# import numpy as np
from collections import Counter
from ruffus import *

import sequence as seq
from Data import getGamesAsEventSeqs
from Event import EventTypes
from utils import printVar
from soccer import removeTouches

class Alphabet(object):
	@staticmethod
	def getAlphabets():
		return (MockAlphabet('a'), MockAlphabet('b'))

	@staticmethod
	def setParams(paramsDict):
		self.params = paramsDict


class MockAlphabet(Alphabet):
	def __init__(self, x):
		self.x = x

	def __str__(self):
		return "Alphabet %s" % (str(self.x))


def main():
	alphabetParams = {'alphafoo': 1, 'alphabar': 2}

	scores1 = {'g': 3, 's': 1}	# goal = 3, shot = 1, all others = 0
	scores2 = {'g': 1}			# goal = 1, all others = 0
	datasetParams = {'seqlengths': (2,4), 'scoring': (scores1, scores2)}

	regressorParams = {'kernels': ('levenshtein', 'p-spectrum'),
		'kernelParams': [5]}

	allParams = regressorParams
	allParams.update(datasetParams)
	allParams.update(alphabetParams)

	# -------------------------------

	allGames = getGamesAsEventSeqs()
	SEQ_LENGTH_BEFORE_SHOT = 2
	shotSeqs2counts = Counter()
	allSeqs2counts = Counter()
	for i, game in enumerate(allGames):
		print("================================")

		game = removeTouches(game)
		labels = map(lambda ev: ev.getType(), game)

		# extract the sequences right before shots
		seqsBeforeShots = seq.extractSubseqsWhere(
			lambda s: s[-1] == EventTypes.SHOT,
			labels, SEQ_LENGTH_BEFORE_SHOT + 1, overlap=True)
		seqsBeforeShots = map(lambda s: s[:-1], seqsBeforeShots)	# remove shot

		# printVar("seqsBeforeShots", seqsBeforeShots)
		shotSeqs2counts.update(seq.uniqueElementCounts(seqsBeforeShots))
		allSeqs2counts.update(seq.uniqueSubseqsCounts(labels,
			SEQ_LENGTH_BEFORE_SHOT))

		# printVar("shotSeqs2counts", shotSeqs2counts)
		# print('-------------------------------')
		# printVar("allseqs2counts", allSeqs2counts)

		if i == 1:
			break

	# NOTE: it looks like we could almost certainly learn "signature patterns"
	# of different teams using tf-idf statistics (or something similar)
		# -or leagues, also
		# -or teams with certain players present/absent
		# -or teams over the course of the season
		# -or teams over the course of a game
		# -and we could prolly also do some baller visualizations showing
		# where on the field stuff was

	allSeqs2fracs = {}
	for s, totalCount in allSeqs2counts.iteritems():
		beforeShotCount = shotSeqs2counts.get(s, 0)
		fractionBeforeShot = float(beforeShotCount) / totalCount
		# print("%s: %g" % (str(s), fractionBeforeShot))
		allSeqs2fracs[s] = fractionBeforeShot

	sortedByFrac = sorted(allSeqs2fracs.items(),
		key=operator.itemgetter(1), reverse=True)

	for el in sortedByFrac:
		sequence = el[0]
		count = allSeqs2counts[sequence]
		frac = el[1]
		seqStr = ', '.join(sequence)
		print("%.3f\t%d\t%s" % (frac, count, seqStr))


# make a dict of params for alphabet
# make a dict of params for dataset construction
	# eg, whether we care about shots, goals, etc, and how good each is
	# also, what range of subseq lengths to use (including step)
# make a dict of params for regressor
# merge all the dicts so it's one big set of parameters
# hand Alphabet its params
# get back an iterator over the resulting alphabets
# for each alphabet
	# ask Data for all the data, converted to strings via the alphabet
	# for each combination of dataset params:
		# construct a dataset from all the strings using those params
		# for each combination of regressor params:
			# compute and store loss (prolly using CV)
			# do...something

if __name__ == '__main__':
	import doctest
	doctest.testmod()

	main()

	# print(Alphabet.getAlphabets())
