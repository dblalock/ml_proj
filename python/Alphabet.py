
import math
from sklearn.base import BaseEstimator, TransformerMixin

from joblib import Memory
memory = Memory('../output', verbose=0)

import Data
from Event import EventTypes

class Alphabet(BaseEstimator, TransformerMixin):

  # Parameters specifying the complexity of the alphabet
  # Playing Zones - number of zones on the pitch (1, 2, 3, 4, 6, 8, 12, 16)
  # pass directions - number of possible pass directions (1, 2, 3, 4)
  # pass lengths - number of possible pass lengths
  # shot locations - number of possible shot locations
  # pass types - number of possible pass types - 1, 2 (headers), 3 (free kicks), 4 (free kicks and headers) - not continuous 
  # shot types - number of possible shot types - 1, 2 (headers), 3 (header and other body parts)
  def __init__(self, success = False, playingZones = 1, passParam = 1, passTypes = 1, shotTypes = 1, shotLocations = 1):
    self.success = success
    self.passSuccess = success
    self.playingZones = playingZones
    self.passParam = passParam
    self.passDirections = passParam
    self.passLengths = passParam
    self.shotLocations = shotLocations
    self.passTypes = passTypes
    self.shotTypes = shotTypes

    self.outcomeEvents = frozenset(["AERIAL", "CLEARANCE", "FOUL", "TACKLE", "TAKE ON"])
    self.nonOutcomeEvents = frozenset(["BALL RECOVERY", "BLOCK", "CHALLENGE", "DISPOSSESSED", "ERROR", "INTERCEPTION", "KEEPER EVENT", "TURNOVER"])

    # self.symbols = self.createSymbols()

  def fit(self, X, y=None, **params):
    return self

  def transform(self, X):
    return self.createDocuments(X)

  # def addTeams(self, teams):
  #   self.teams = teams

  def createSymbols(self):
    shotSymbols = self.createShotSymbols()

    unsuccessfullSymbols = [x + '_0' for x in self.outcomeEvents]

    if self.success:
      successSymbols = [x + '_1' for x in self.outcomeEvents]
    else:
      successSymbols = []
    

    # PASSC - Corner
    if self.passTypes == 1:
      passSymbols = ["PASS_0"]
    elif self.passTypes == 2 or self.passTypes == 3:
      passSymbols = ["PASS_0", "PASS_1"]
    elif self.passTypes == 4:
      passSymbols = ["PASS_0", "PASS_1", "PASS_2"]
    cornerSymbols = ["PASS_C"]

    passLengthSymbols = []
    cornerLengthSymbols = []
    for i in range(self.passLengths):
      passLengthSymbols = passLengthSymbols + [x + '_' + str(i) for x in passSymbols]
      cornerLengthSymbols.append("PASS_C_" + str(i) +"_0")

    passDirectionSymbols = []
    for i in range(self.passDirections):
      passDirectionSymbols = passDirectionSymbols + [x + '_' + str(i) for x in passLengthSymbols]

    passUnsuccessfullSymbols = [x + '_0' for x in passDirectionSymbols]
    cornerUnsuccessfullSymbols = [x + '_0_0' for x in cornerLengthSymbols]
    if self.passSuccess:
      passSuccessSymbols = [x + '_1' for x in passDirectionSymbols]
      cornerSuccessSymbols = [x + '_1_0' for x in cornerLengthSymbols]
    else:
      passSuccessSymbols = []
      cornerSuccessSymbols = []

    locationSymbols = []

    for i in range(self.playingZones):
      locationSymbols += [x + '_' + str(i) for x in successSymbols + unsuccessfullSymbols]
      locationSymbols += [x + '_' + str(i) for x in self.nonOutcomeEvents]
      locationSymbols += [x + '_' + str(i) for x in passSuccessSymbols + passUnsuccessfullSymbols]

    locationSymbols += cornerSuccessSymbols + cornerUnsuccessfullSymbols + shotSymbols

    symbolDictionary = {}

    for s in locationSymbols:
      symbolDictionary[s] = Symbol(s)

    return symbolDictionary

  def createShotSymbols(self):
    s = "SHOT"

    shotTypeSymbols = [s + '_' + str(i) for i in range(self.shotTypes)]
    shotOnTargetSymbols = [x + '_0' for x in shotTypeSymbols]
    shotOnTargetSymbols += [x + '_1' for x in shotTypeSymbols]
    shotOnTargetSymbols += ["SHOT_F_0", "SHOT_F_1"]

    shotLocationSymbols = []
    for i in range(self.shotLocations):
      shotLocationSymbols += [x + '_' + str(i) for x in shotOnTargetSymbols]

    shotLocationSymbols += ["SHOT_G", "SHOT_P_0", "SHOT_P_1"]

    return shotLocationSymbols


  # teams - dictionary mapping teamA: "A", teamB: "B"
  def createSymbolFromEvent(self, e):
    # print("createSymbolFromEvent: team = %s" % e.team)

    # t = self.teams.get(e.team, "B")

    if str(e) in self.outcomeEvents:
      s = self.createOutcomeSymbol(e)

    elif str(e) in self.nonOutcomeEvents:
      s = self.createNonOutcomeSymbol(e)

    elif str(e) == "PASS":
      s = self.createPassSymbol(e)

    elif str(e) == "SHOT":
      s = self.createShotSymbol(e)

    return s
    # return self.symbols[s]

  def createNonOutcomeSymbol(self, e):
    s = str(e)
    z = determineZone(self.playingZones, e)
    s = s + '_' + z
    return s

  def createOutcomeSymbol(self, e):
    o = '0'
    if self.success:
      if e.outcome:
        o = '1'
    z = determineZone(self.playingZones, e)
    s = str(e) + '_' + o + '_' + z
    return s

  def createPassSymbol(self, e):
    s = str(e)

    o = '0'
    if self.passSuccess:
      if e.outcome:
        o = '1'

    if e.corner:
      t = 'C'
      d = '0'
      loc = '0'
      l = determinePassLength(self.passLengths, e)

    else:
      loc = determineZone(self.playingZones, e)
      l = determinePassLength(self.passLengths, e)
      d = determinePassDirection(self.passDirections, e)
      t = determinePassType(self.passTypes, e)

    return s + '_' + t + '_' + l + '_' + d + '_' + o + '_' + loc

  def createShotSymbol(self, e):
    s = str(e)

    if e.goal:
      return s + '_G'

    if e.on_target:
      o = '1'
    else:
      o = '0'

    if e.penalty:
      return s + '_P_' + o

    elif e.free_kick:
      t = 'F'
      l = determineShotLocation(self.shotLocations, e)

    else:
      t = determineShotType(self.shotTypes, e)
      l = determineShotLocation(self.shotLocations, e)

    return s + '_' + t + '_' + o + '_' + l

  # @memory.cache
  def createDocuments(self, games):
    docs = []
    for game in games:
      # call this in a bizarre way (not self.createDocument(game)) to
      # bypass what seems to be a bug in joblib...
      docs.append(Alphabet.createDocument(self, game))
    return docs

  @memory.cache
  def createDocument(self, game):

    # if we're given a string, assume it's a filename
    # storing the events of the game
    if isinstance(game, basestring):
      print("Creating document from game: %s", game)
      events = Data.readEvents(game)
      events = filter(lambda ev: ev.getType() != EventTypes.TOUCH, events)
    else:
      events = game

    # teams = getTeams(events)
    # self.addTeams(teams)

    # print(self.symbols)

    seq = []
    for e in events:
      if str(e) != "ATTACKING MOVE" and str(e) != "TOUCH":
        s = str(self.createSymbolFromEvent(e))
        s = s.replace(' ', '')
        seq.append(s)

    return ' '.join(seq)

# Basic Symbols
class Symbol():

  def __init__(self, name):
    self.name = name

  def __str__(self):
    return self.name


def determineShotLocation(locations, e):
  return '0'

def determineShotType(types, e):
  if types == 1:
    return '0'

  elif types == 2:
    if e.header:
      return '1'
    else:
      return '0'

  elif types == 3:
    if e.other_body_part:
      return '2'
    elif e.header:
      return '1'
    else:
      return '0'

  else:
    raise ValueError('shotTypes not an acceptable value')

def determinePassType(types, e):
  if types == 1:
    return '0'

  elif types == 2:
    if e.header:
      return '1'
    return '0'

  elif types == 3:
    if e.free_kick:
      return '1'
    return '0'

  elif types == 4:
    if e.header:
      return '1'
    elif e.free_kick:
      return '2'
    else:
      return '0'

  else:
    raise ValueError('passTypes not an acceptable value')

def determinePassDirection(directions, e):

  FLIMIT = math.pi / 3.0

  if directions == 1:
    return '0'

  elif directions == 2:
    if (math.pi / 2.0) < e.angle <= (3.0 * math.pi / 2.0):
      return '0'
    else:
      return '1'

  elif directions == 3:
    if (math.pi / 2.0) < e.angle <= (3.0 * math.pi / 2.0):
      return '0'
    else:
      if e.angle < (math.pi / 2.0):
        return '1'
      else:
        return '2'

  elif directions == 4:
    if (math.pi - FLIMIT) < e.angle <= (math.pi + FLIMIT):
      return '0'
    elif FLIMIT < e.angle <= (math.pi - FLIMIT):
      return '1'
    elif e.angle <= FLIMIT or e.angle > (2.0 * math.pi - FLIMIT):
      return '2'
    else:
      return '3'

  else:
    raise ValueError('passDirections not an acceptable value')


def determinePassLength(lengths, e):
  if lengths == 1:
    return '0'

  MAXL = 10.0
  if e.distance >= MAXL:
    return str(lengths - 1)
  else:
    b = MAXL / float(lengths - 1) 
    return str(int(e.distance / b))

def determineZone(zones, e):
  if zones == 1:
    return '0'

  elif zones == 2:
    if e.start_x <= 50.0:
      return '0'
    else:
      return '1'

  elif zones == 3:
    if e.start_x <= 50.0:
      return '0'
    else:
      if e.start_y <= 50.0:
        return '1'
      else:
        return '2'

  elif zones == 4:
    if e.start_x <= 50.0:
      return '0'
    else:
      if e.start_y <= 21.1:
        return '1'
      elif 21.1 < e.start_y <= 78.9:
        return '2'
      else:
        return '3'

  elif zones == 6:
    if e.start_x <= 50.0:
      if e.start_y <= 21.1:
        return '0'
      elif 21.1 < e.start_y <= 78.9:
        return '1'
      else:
        return '2'
    else:
      if e.start_y <= 21.1:
        return '3'
      elif 21.1 < e.start_y <= 78.9:
        return '4'
      else:
        return '5'

  elif zones == 8:
    if e.start_x <= 17.0 and 21.1 < e.start_y <= 78.9:
      return '0'
    elif e.start_x > 83.0 and 21.1 < e.start_y <= 78.9:
      return '1'
    else:
      if e.start_x <= 50.0:
        if e.start_y <= 21.1:
          return '2'
        elif 21.1 < e.start_y <= 78.9:
          return '3'
        else:
          return '4'
      else:
        if e.start_y <= 21.1:
          return '5'
        elif 21.1 < e.start_y <= 78.9:
          return '6'
        else:
          return '7'

  elif zones == 12:
    if e.start_x <= 17.0 and 21.1 < e.start_y <= 78.9:
      return '0'
    elif e.start_x > 83.0 and 21.1 < e.start_y <= 78.9:
      return '1'
    else:
      if e.start_x <= 17.0:
        if e.start_y <= 21.1:
          return '2'
        else:
          return '3'
      elif 17.0 < e.start_x <= 50.0:
        if e.start_y <= 21.1:
          return '4'
        elif 21.1 < e.start_y <= 78.9:
          return '5'
        else:
          return '6'
      elif 50.0 < e.start_x <= 83.0:
        if e.start_y <= 21.1:
          return '7'
        elif 21.1 < e.start_y <= 78.9:
          return '8'
        else:
          return '9'
      else:
        if e.start_y <= 21.1:
          return '10'
        else:
          return '11'

  elif zones == 16:
    if e.start_x <= 17.0 and 21.1 < e.start_y <= 78.9:
      if e.start_y <= 36.8:
        return '0'
      elif 36.8 < e.start_y <= 63.2:
        return '1'
      else:
        return '2'
    elif e.start_x > 83.0 and 21.1 < e.start_y <= 78.9:
      if e.start_y <= 36.8:
        return '3'
      elif 36.8 < e.start_y <= 63.2:
        return '4'
      else:
        return '5'
    else:
      if e.start_x <= 17.0:
        if e.start_y <= 21.1:
          return '6'
        else:
          return '7'
      elif 17.0 < e.start_x <= 50.0:
        if e.start_y <= 21.1:
          return '8'
        elif 21.1 < e.start_y <= 78.9:
          return '9'
        else:
          return '10'
      elif 50.0 < e.start_x <= 83.0:
        if e.start_y <= 21.1:
          return '11'
        elif 21.1 < e.start_y <= 78.9:
          return '12'
        else:
          return '13'
      else:
        if e.start_y <= 21.1:
          return '14'
        else:
          return '15'

  else:
    raise ValueError('playingZones not an acceptable value')


########################################### Data to Document ###############################################

def getTeams(events):
  a = events[0].team
  # for e in events:
  #   if e.team != a:
  #     b = e.team
  #     return {a: "A", b:"B"}
  return {a: "A"}

# Possible Parameter Values
successValues = [True, False]
passSuccessValues = [True, False]
playingZoneValues = [1, 2, 3, 4, 6, 8, 12, 16]
passDirectionValues = [1, 2, 3, 4]
passTypeValues = [1, 2, 3, 4]
shotTypeValues = [1, 2, 3]
shotLocationValues = [1]
# Pass Lengths Continuous (Ideally ~4)
passLengthValues = [1 + x for x in range(10)]

# Reduced search space
# success and passSuccess tied, passDirection and passLength  
def createAlphabets(successParams = [True, False], zoneParams = playingZoneValues, passParams = [1, 2, 3, 4], passTypeParams = passTypeValues, shotTypeParams = shotTypeValues, shotLocationParams = shotLocationValues):
  alphabets = []
  for s in successParams:
    for z in zoneParams:
      for p in passParams:
        for pt in passTypeParams:
          for st in shotTypeParams:
            for sl in shotLocationParams:
              alphabets.append(Alphabet(success = s, passSuccess = s, playingZones = z, passDirections = p, passLengths = p, passTypes = pt, shotTypes = st, shotLocations = sl))

  return alphabets


